/*-
 * Copyright (C) 2011, 2012 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/fbio.h>
#include <sys/consio.h>
#include <fcntl.h>
#include <unistd.h>

#include "ps3gpu_ctl.h"
#include "ps3gpu_mth.h"
#include "reset_gpu_state.h"
#include "util.h"

int
main(int argc, char **argv)
{
	struct ps3gpu_ctl_context_allocate context_allocate;
	struct ps3gpu_ctl_context_free context_free;
	int context_id;
	volatile uint32_t *control;
	volatile uint8_t *driver_info;
	uint32_t *fifo, *reset_gpu, *db[2], *zb;
	unsigned long fifo_handle, reset_gpu_handle, db_handle[2], zb_handle;
	unsigned int fifo_gaddr, reset_gpu_gaddr, db_gaddr[2], zb_gaddr;
	int fd = -1;
	int i, err;

	/* Open GPU device */

	fd = open(PS3GPU_DEV_PATH, O_RDWR);
	if (fd < 0) {
		perror("open");
		goto done;
	}

	/* Create GPU context */

	context_allocate.vram_size = 64; /* MB */

	err = ioctl(fd, PS3GPU_CTL_CONTEXT_ALLOCATE, &context_allocate);
	if (err < 0) {
		perror("ioctl");
		goto done;
	}

	context_id = context_allocate.context_id;

	printf("context id %d\n", context_id);
	printf("control handle 0x%lx size %d\n",
	    context_allocate.control_handle, context_allocate.control_size);
	printf("driver_info handle 0x%lx size %d\n",
	    context_allocate.driver_info_handle, context_allocate.driver_info_size);

	/* Map control registers */

	control = mmap(NULL, context_allocate.control_size,
	    PROT_READ | PROT_WRITE, MAP_SHARED, fd, context_allocate.control_handle);
	if (control == (void *) MAP_FAILED) {
		perror("mmap");
		goto done;
	}

	/* Map driver info */

	driver_info = mmap(NULL, context_allocate.driver_info_size,
	    PROT_READ | PROT_WRITE, MAP_SHARED, fd, context_allocate.driver_info_handle);
	if (driver_info == (void *) MAP_FAILED) {
		perror("mmap");
		goto done;
	}

	printf("channel id %d\n", get_channel_id(driver_info));
	printf("flip status %d %d\n", get_flip_status(driver_info, 0), get_flip_status(driver_info, 1));
	
	/* Set flip mode */

	err = set_flip_mode(fd, context_id, PS3GPU_CTL_HEAD_A, PS3GPU_CTL_FLIP_MODE_VSYNC);
	if (err < 0) {
		perror("set_flip_mode");
		goto done;
	}

	err = set_flip_mode(fd, context_id, PS3GPU_CTL_HEAD_B, PS3GPU_CTL_FLIP_MODE_VSYNC);
	if (err < 0) {
		perror("set_flip_mode");
		goto done;
	}

	/* Reset flip status */

	err = reset_flip_status(fd, context_id, PS3GPU_CTL_HEAD_A);
	if (err < 0) {
		perror("reset_flip_status");
		goto done;
	}

	err = reset_flip_status(fd, context_id, PS3GPU_CTL_HEAD_B);
	if (err < 0) {
		perror("reset_flip_status");
		goto done;
	}

	/* Allocate FIFO */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_GART,
	    64 * 1024, 12, &fifo_handle, &fifo_gaddr, (void **) &fifo);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("FIFO handle 0x%lx gpu addr 0x%08x\n",
	    fifo_handle, fifo_gaddr);

	/* Setup FIFO */

	err = setup_control(fd, context_id, fifo_handle, fifo_handle, 0xdeadbabe);
	if (err < 0) {
		perror("setup_control");
		goto done;
	}

	printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
	    control[0x10], control[0x11], control[0x12]);

	/* Allocate FIFO for resetting GPU state */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_GART,
	    4 * 1024, 12, &reset_gpu_handle, &reset_gpu_gaddr, (void **) &reset_gpu);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("reset GPU state handle 0x%lx gpu addr 0x%08x\n",
	    reset_gpu_handle, reset_gpu_gaddr);

	memcpy(reset_gpu, reset_gpu_state_3d, reset_gpu_state_3d_size);

	/* Kick FIFO */

	fifo[0] = PS3GPU_MTH_HDR(0, 0, reset_gpu_gaddr | PS3GPU_MTH_ADDR_CALL);
	fifo[1] = PS3GPU_MTH_HDR(1, 0, PS3GPU_MTH_ADDR_REF);
	fifo[2] = 0xcafef00d;

	control[0x10] = fifo_gaddr + 3 * sizeof(uint32_t);

	err = wait_fifo_idle(control);
	if (err < 0) {
		fprintf(stderr, "FIFO timeout: put 0x%08x get 0x%08x ref 0x%08x\n",
		    control[0x10], control[0x11], control[0x12]);
		dump_fifo(stderr, fifo, 0x400);
		goto done;
	}

	printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
	    control[0x10], control[0x11], control[0x12]);

	/* Allocate display buffers */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_VIDEO,
	    ROUNDUP(DISPLAY_HEIGHT * DISPLAY_PITCH, 4 * 1024), 12,
	    &db_handle[0], &db_gaddr[0], (void **) &db[0]);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("DB0 handle 0x%lx gpu addr 0x%08x\n",
	    db_handle[0], db_gaddr[0]);

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_VIDEO,
	    ROUNDUP(DISPLAY_HEIGHT * DISPLAY_PITCH, 4 * 1024), 12,
	    &db_handle[1], &db_gaddr[1], (void **) &db[1]);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("DB1 handle 0x%lx gpu addr 0x%08x\n",
	    db_handle[1], db_gaddr[1]);

	/* Allocate depth buffer */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_VIDEO,
	    ROUNDUP(DISPLAY_HEIGHT * DISPLAY_PITCH, 4 * 1024), 12,
	    &zb_handle, &zb_gaddr, (void **) &zb);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("ZB handle 0x%lx gpu addr 0x%08x\n",
	    zb_handle, zb_gaddr);

	/* Set display buffers */

	err = display_buffer_set(fd, context_id, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT,
	    DISPLAY_PITCH, db_handle[0]);
	if (err < 0) {
		perror("display_buffer_set");
		goto done;
	}

	err = display_buffer_set(fd, context_id, 1, DISPLAY_WIDTH, DISPLAY_HEIGHT,
	    DISPLAY_PITCH, db_handle[1]);
	if (err < 0) {
		perror("display_buffer_set");
		goto done;
	}

	const struct surface_desc surf_desc[] = {
		/* display buffer 0 */
		{
			.sd_color_loc = { 0xfeed0000, 0xfeed0000, 0xfeed0000, 0xfeed0000 },
			.sd_color_off = { db_gaddr[0], 0, 0, 0 },
			.sd_color_pitch = { DISPLAY_PITCH, 64, 64, 64 },
			.sd_color_fmt = 0x8,
			.sd_color_target = 0x1,
			.sd_depth_loc = 0xfeed0000,
			.sd_depth_off = zb_gaddr,
			.sd_depth_pitch = DISPLAY_PITCH,
			.sd_depth_fmt = 0x2,
			.sd_x = 0,
			.sd_y = 0,
			.sd_w = DISPLAY_WIDTH,
			.sd_h = DISPLAY_HEIGHT,
		},
		/* display buffer 1 */
		{
			.sd_color_loc = { 0xfeed0000, 0xfeed0000, 0xfeed0000, 0xfeed0000 },
			.sd_color_off = { db_gaddr[1], 0, 0, 0 },
			.sd_color_pitch = { DISPLAY_PITCH, 64, 64, 64 },
			.sd_color_fmt = 0x8,
			.sd_color_target = 0x1,
			.sd_depth_loc = 0xfeed0000,
			.sd_depth_off = zb_gaddr,
			.sd_depth_pitch = DISPLAY_PITCH,
			.sd_depth_fmt = 0x2,
			.sd_x = 0,
			.sd_y = 0,
			.sd_w = DISPLAY_WIDTH,
			.sd_h = DISPLAY_HEIGHT,
		},
	};

	const uint32_t clear_color[] = {
		0xff00ff00,
		0xffffff00,
	};

	for (i = 0; i < ARRAY_SIZE(surf_desc); i++) {
		err = setup_control(fd, context_id, fifo_handle, fifo_handle, 0xdeadbabe);
		if (err < 0) {
			perror("setup_control");
			goto done;
		}

		err += set_surface(fifo + err, &surf_desc[i]);
		err += set_depth_mask(fifo + err, 0x00000000);
		err += set_color_mask(fifo + err, 0x01010101);
		err += set_color_mask_mrt(fifo + err, 0x00000000);
		err += set_clear_color(fifo + err, clear_color[i]);
		err += set_scissor(fifo + err, 0, 0, 4095, 4095);
		err += clear_surface(fifo + err, 0x000000f1);

		err += flip_display_buffer(fifo + err, get_channel_id(driver_info), i, 0);

		/*
		 * Label with index 0 (head 0) is set by LV1 to 0x00000000 when flip is complete.
		 * Let GPU wait for it.
		 */

		err += wait_label(fifo + err, 0, 0x00000000);

		control[0x10] = fifo_gaddr + err * sizeof(uint32_t);

		err = wait_fifo_idle(control);
		if (err < 0) {
			fprintf(stderr, "FIFO timeout: put 0x%08x get 0x%08x ref 0x%08x\n",
			    control[0x10], control[0x11], control[0x12]);
			dump_fifo(stderr, fifo, 0x400);
			goto done;
		}

		printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
		    control[0x10], control[0x11], control[0x12]);

		usleep(1000000);
	}

	/* Destroy GPU context */

	context_free.context_id = context_id;

	err = ioctl(fd, PS3GPU_CTL_CONTEXT_FREE, &context_free);
	if (err < 0) {
		perror("ioctl");
		goto done;
	}

done:

	if (fd >= 0)
		close(fd);

	/* Restore console */

	ioctl(0, SW_TEXT_80x25, NULL);

	exit(0);
}
