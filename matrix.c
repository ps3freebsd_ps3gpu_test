/*-
 * Copyright (C) 2011, 2012 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#include <string.h>

#include "matrix.h"

void
matrix_ident(matrix_t m)
{
	memset(m, 0, MATRIX_SIZE);
	MATRIX_ELEM(m, 0, 0) = 1.0f;
	MATRIX_ELEM(m, 1, 1) = 1.0f;
	MATRIX_ELEM(m, 2, 2) = 1.0f;
	MATRIX_ELEM(m, 3, 3) = 1.0f;
}

void
matrix_trans(matrix_t m, float x, float y, float z)
{
	matrix_ident(m);
	MATRIX_ELEM(m, 0, 3) = x;
	MATRIX_ELEM(m, 1, 3) = y;
	MATRIX_ELEM(m, 2, 3) = z;
}

void
matrix_mult(matrix_t m, const matrix_t m1, const matrix_t m2)
{
	int i, j;

	for (i = 0; i < 4; i++) {
		for (j = 0; j < 4; j++) {
			MATRIX_ELEM(m, i, j) =
			    MATRIX_ELEM(m1, i, 0) * MATRIX_ELEM(m2, 0, j) +
			    MATRIX_ELEM(m1, i, 1) * MATRIX_ELEM(m2, 1, j) +
			    MATRIX_ELEM(m1, i, 2) * MATRIX_ELEM(m2, 2, j) +
			    MATRIX_ELEM(m1, i, 3) * MATRIX_ELEM(m2, 3, j);
		}
	}
}

void
matrix_proj(matrix_t m, float top, float bottom, float left, float right,
    float near, float far)
{
	memset(m, 0, MATRIX_SIZE); 
	MATRIX_ELEM(m, 0, 0) = (2.0f * near) / (right - left);
	MATRIX_ELEM(m, 0, 2) = (right + left) / (right - left);
	MATRIX_ELEM(m, 1, 1) = (2.0f * near) / (bottom - top);
	MATRIX_ELEM(m, 1, 2) = (top + bottom) / (top - bottom);
	MATRIX_ELEM(m, 2, 2) = -(far + near) / (far - near);
	MATRIX_ELEM(m, 2, 3) = -(2.0f *far * near) / (far - near);
	MATRIX_ELEM(m, 3, 2) = -1.0f; 
}
