
CC=gcc
CFLAGS=-Wall -O2 -g -fno-strict-aliasing
CFLAGS+=-DPS3GPU_DEV_PATH=\"/dev/ps3gpu\"
CFLAGS+=-DDISPLAY_WIDTH=1920 -DDISPLAY_HEIGHT=1080 -DDISPLAY_BPP=4
CFLAGS+=-DDISPLAY_PITCH=\(DISPLAY_WIDTH*DISPLAY_BPP\)
LDFLAGS=
SRC=util.c matrix.c reset_gpu_state.c
OBJ=$(SRC:.c=.o)
LIB=-lm

all: cursor vram_dma gart_dma inline label display_buffer solid \
	triangle quad vertex_buffer model_view_proj texture \
	timestamp

cursor: $(OBJ) cursor.o
	$(CC) $(LDFLAGS) -o $@ $(OBJ) cursor.o $(LIB)

vram_dma: $(OBJ) vram_dma.o
	$(CC) $(LDFLAGS) -o $@ $(OBJ) vram_dma.o $(LIB)

gart_dma: $(OBJ) gart_dma.o
	$(CC) $(LDFLAGS) -o $@ $(OBJ) gart_dma.o $(LIB)

inline: $(OBJ) inline.o
	$(CC) $(LDFLAGS) -o $@ $(OBJ) inline.o $(LIB)

label: $(OBJ) label.o
	$(CC) $(LDFLAGS) -o $@ $(OBJ) label.o $(LIB)

display_buffer: $(OBJ) display_buffer.o
	$(CC) $(LDFLAGS) -o $@ $(OBJ) display_buffer.o $(LIB)

solid: $(OBJ) solid.o
	$(CC) $(LDFLAGS) -o $@ $(OBJ) solid.o $(LIB)

triangle: $(OBJ) triangle.o
	$(CC) $(LDFLAGS) -o $@ $(OBJ) triangle.o $(LIB)

quad: $(OBJ) quad.o
	$(CC) $(LDFLAGS) -o $@ $(OBJ) quad.o $(LIB)

vertex_buffer: $(OBJ) vertex_buffer.o
	$(CC) $(LDFLAGS) -o $@ $(OBJ) vertex_buffer.o $(LIB)

model_view_proj: $(OBJ) model_view_proj.o
	$(CC) $(LDFLAGS) -o $@ $(OBJ) model_view_proj.o $(LIB)

texture: $(OBJ) texture.o
	$(CC) $(LDFLAGS) -o $@ $(OBJ) texture.o $(LIB)

timestamp: $(OBJ) timestamp.o
	$(CC) $(LDFLAGS) -o $@ $(OBJ) timestamp.o $(LIB)

%.o: %.c
	$(CC) $(CFLAGS) -c $<

clean:
	rm -f $(OBJ)
	rm -f cursor.o cursor
	rm -f vram_dma.o vram_dma
	rm -f gart_dma.o gart_dma
	rm -f inline.o inline
	rm -f label.o label
	rm -f display_buffer.o display_buffer
	rm -f solid.o solid
	rm -f triangle.o triangle
	rm -f quad.o quad
	rm -f vertex_buffer.o vertex_buffer
	rm -f model_view_proj.o model_view_proj
	rm -f texture.o texture
	rm -f timestamp.o timestamp
