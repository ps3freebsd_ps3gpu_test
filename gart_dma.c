/*-
 * Copyright (C) 2011, 2012 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/fbio.h>
#include <sys/consio.h>
#include <fcntl.h>
#include <unistd.h>

#include "ps3gpu_ctl.h"
#include "ps3gpu_mth.h"
#include "reset_gpu_state.h"
#include "util.h"

int
main(int argc, char **argv)
{
	struct ps3gpu_ctl_context_allocate context_allocate;
	struct ps3gpu_ctl_context_free context_free;
	int context_id;
	volatile uint32_t *control;
	uint32_t *fifo, *reset_gpu, *vram, *gart;
	unsigned long fifo_handle, reset_gpu_handle, vram_handle, gart_handle;
	unsigned int fifo_gaddr, reset_gpu_gaddr, vram_gaddr, gart_gaddr;
	int fd = -1;
	int x, y;
	int w, h;
	int err;

	/* Open GPU device */

	fd = open(PS3GPU_DEV_PATH, O_RDWR);
	if (fd < 0) {
		perror("open");
		goto done;
	}

	/* Create GPU context */

	context_allocate.vram_size = 64; /* MB */

	err = ioctl(fd, PS3GPU_CTL_CONTEXT_ALLOCATE, &context_allocate);
	if (err < 0) {
		perror("ioctl");
		goto done;
	}

	context_id = context_allocate.context_id;

	printf("context id %d\n", context_id);
	printf("control handle 0x%lx size %d\n",
	    context_allocate.control_handle, context_allocate.control_size);

	/* Map control registers */

	control = mmap(NULL, context_allocate.control_size,
	    PROT_READ | PROT_WRITE, MAP_SHARED, fd, context_allocate.control_handle);
	if (control == (void *) MAP_FAILED) {
		perror("mmap");
		goto done;
	}

	/* Allocate FIFO */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_GART,
	    64 * 1024, 12, &fifo_handle, &fifo_gaddr, (void **) &fifo);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("FIFO handle 0x%lx gpu addr 0x%08x\n",
	    fifo_handle, fifo_gaddr);

	/* Setup FIFO */

	err = setup_control(fd, context_id, fifo_handle, fifo_handle, 0xdeadbabe);
	if (err < 0) {
		perror("setup_control");
		goto done;
	}

	printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
	    control[0x10], control[0x11], control[0x12]);

	/* Allocate FIFO for resetting GPU state */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_GART,
	    4 * 1024, 12, &reset_gpu_handle, &reset_gpu_gaddr, (void **) &reset_gpu);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("reset GPU state handle 0x%lx gpu addr 0x%08x\n",
	    reset_gpu_handle, reset_gpu_gaddr);

	memcpy(reset_gpu, reset_gpu_state, reset_gpu_state_size);

	/* Kick FIFO */

	fifo[0] = PS3GPU_MTH_HDR(0, 0, reset_gpu_gaddr | PS3GPU_MTH_ADDR_CALL);
	fifo[1] = PS3GPU_MTH_HDR(1, 0, PS3GPU_MTH_ADDR_REF);
	fifo[2] = 0xcafef00d;

	control[0x10] = fifo_gaddr + 3 * sizeof(uint32_t);

	err = wait_fifo_idle(control);
	if (err < 0) {
		fprintf(stderr, "FIFO timeout: put 0x%08x get 0x%08x ref 0x%08x\n",
		    control[0x10], control[0x11], control[0x12]);
		dump_fifo(stderr, fifo, 0x400);
		goto done;
	}

	printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
	    control[0x10], control[0x11], control[0x12]);

	/* Allocate VRAM */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_VIDEO,
	    ROUNDUP(DISPLAY_HEIGHT * DISPLAY_PITCH, 4 * 1024), 12,
	    &vram_handle, &vram_gaddr, (void **) &vram);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("VRAM handle 0x%lx gpu addr 0x%08x\n",
	    vram_handle, vram_gaddr);

	memset32(vram, 0xff404040, DISPLAY_HEIGHT * DISPLAY_WIDTH);

	/* Flip */

	err = flip(fd, context_id, PS3GPU_CTL_HEAD_A, vram_handle);
	if (err < 0) {
		perror("flip");
		goto done;
	}

	err = flip(fd, context_id, PS3GPU_CTL_HEAD_B, vram_handle);
	if (err < 0) {
		perror("flip");
		goto done;
	}

	usleep(2000000);

	/* Allocate GART */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_GART,
	    1024 * 1024, 12, &gart_handle, &gart_gaddr, (void **) &gart);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("GART handle 0x%lx gpu addr 0x%08x\n",
	    gart_handle, gart_gaddr);

	/* Test RSX DMA */

	w = 200;
	h = 200;

	for (y = 0; y < h; y++) {
		for (x = 0; x < w; x++) {
			if (y < (h / 2))
				gart[y * w + x] = 0xff00ff00;
			else
				gart[y * w + x] = 0xffffff00;
		}
	}

	err = setup_control(fd, context_id, fifo_handle, fifo_handle, 0xdeadbabe);
	if (err < 0) {
		perror("setup_control");
		goto done;
	}

	x = 200;
	y = 800;

	err = transfer_data(fifo, 0xfeed0001, 0xfeed0000,
	    vram_gaddr + y * DISPLAY_PITCH + x * DISPLAY_BPP, DISPLAY_PITCH,
	    gart_gaddr + 0, w * DISPLAY_BPP,
	    w * DISPLAY_BPP, h);

	x = 800;
	y = 800;

	err += transfer_data(fifo + err, 0xfeed0001, 0xfeed0000,
	    vram_gaddr + y * DISPLAY_PITCH + x * DISPLAY_BPP, DISPLAY_PITCH,
	    gart_gaddr + 0, w * DISPLAY_BPP,
	    w * DISPLAY_BPP, h);

	control[0x10] = fifo_gaddr + err * sizeof(uint32_t);

	err = wait_fifo_idle(control);
	if (err < 0) {
		fprintf(stderr, "FIFO timeout: put 0x%08x get 0x%08x ref 0x%08x\n",
		    control[0x10], control[0x11], control[0x12]);
		dump_fifo(stderr, fifo, 0x400);
		goto done;
	}

	printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
	    control[0x10], control[0x11], control[0x12]);

	save_image("image.argb", (const char *) vram, DISPLAY_PITCH * DISPLAY_HEIGHT);

	/* Destroy GPU context */

	context_free.context_id = context_id;

	err = ioctl(fd, PS3GPU_CTL_CONTEXT_FREE, &context_free);
	if (err < 0) {
		perror("ioctl");
		goto done;
	}

done:

	if (fd >= 0)
		close(fd);

	/* Restore console */

	ioctl(0, SW_TEXT_80x25, NULL);

	exit(0);
}
