/*-
 * Copyright (C) 2011, 2012 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/fbio.h>
#include <sys/consio.h>
#include <fcntl.h>
#include <unistd.h>

#include "ps3gpu_ctl.h"
#include "ps3gpu_mth.h"
#include "reset_gpu_state.h"
#include "util.h"

int
main(int argc, char **argv)
{
	struct ps3gpu_ctl_context_allocate context_allocate;
	struct ps3gpu_ctl_context_free context_free;
	int context_id;
	volatile uint32_t *control;
	volatile uint8_t *driver_info;
	uint32_t *fifo, *reset_gpu, *db[2], *zb, *fp;
	unsigned long fifo_handle, reset_gpu_handle, db_handle[2], zb_handle, fp_handle;
	unsigned int fifo_gaddr, reset_gpu_gaddr, db_gaddr[2], zb_gaddr, fp_gaddr;
	int fd = -1;
	int i, err;

	/* Open GPU device */

	fd = open(PS3GPU_DEV_PATH, O_RDWR);
	if (fd < 0) {
		perror("open");
		goto done;
	}

	/* Create GPU context */

	context_allocate.vram_size = 64; /* MB */

	err = ioctl(fd, PS3GPU_CTL_CONTEXT_ALLOCATE, &context_allocate);
	if (err < 0) {
		perror("ioctl");
		goto done;
	}

	context_id = context_allocate.context_id;

	printf("context id %d\n", context_id);
	printf("control handle 0x%lx size %d\n",
	    context_allocate.control_handle, context_allocate.control_size);
	printf("driver_info handle 0x%lx size %d\n",
	    context_allocate.driver_info_handle, context_allocate.driver_info_size);

	/* Map control registers */

	control = mmap(NULL, context_allocate.control_size,
	    PROT_READ | PROT_WRITE, MAP_SHARED, fd, context_allocate.control_handle);
	if (control == (void *) MAP_FAILED) {
		perror("mmap");
		goto done;
	}

	/* Map driver info */

	driver_info = mmap(NULL, context_allocate.driver_info_size,
	    PROT_READ | PROT_WRITE, MAP_SHARED, fd, context_allocate.driver_info_handle);
	if (driver_info == (void *) MAP_FAILED) {
		perror("mmap");
		goto done;
	}

	printf("channel id %d\n", get_channel_id(driver_info));

	/* Allocate FIFO */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_GART,
	    64 * 1024, 12, &fifo_handle, &fifo_gaddr, (void **) &fifo);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("FIFO handle 0x%lx gpu addr 0x%08x\n",
	    fifo_handle, fifo_gaddr);

	/* Setup FIFO */
	
	err = setup_control(fd, context_id, fifo_handle, fifo_handle, 0xdeadbabe);
	if (err < 0) {
		perror("setup_control");
		goto done;
	}

	printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
	    control[0x10], control[0x11], control[0x12]);

	/* Allocate FIFO for resetting GPU state */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_GART,
	    4 * 1024, 12, &reset_gpu_handle, &reset_gpu_gaddr, (void **)&reset_gpu);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("reset GPU state handle 0x%lx gpu addr 0x%08x\n",
	    reset_gpu_handle, reset_gpu_gaddr);

	memcpy(reset_gpu, reset_gpu_state_3d, reset_gpu_state_3d_size);

	/* Kick FIFO */

	fifo[0] = PS3GPU_MTH_HDR(0, 0, reset_gpu_gaddr | PS3GPU_MTH_ADDR_CALL);
	fifo[1] = PS3GPU_MTH_HDR(1, 0, PS3GPU_MTH_ADDR_REF);
	fifo[2] = 0xcafef00d;

	control[0x10] = fifo_gaddr + 3 * sizeof(uint32_t);

	err = wait_fifo_idle(control);
	if (err < 0) {
		fprintf(stderr, "FIFO timeout: put 0x%08x get 0x%08x ref 0x%08x\n",
		    control[0x10], control[0x11], control[0x12]);
		dump_fifo(stderr, fifo, 0x400);
		goto done;
	}

	printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
	    control[0x10], control[0x11], control[0x12]);

	/* Allocate display buffers */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_VIDEO,
	    ROUNDUP(DISPLAY_HEIGHT * DISPLAY_PITCH, 4 * 1024), 12,
	    &db_handle[0], &db_gaddr[0], (void **) &db[0]);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("DB0 handle 0x%lx gpu addr 0x%08x\n",
	    db_handle[0], db_gaddr[0]);

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_VIDEO,
	    ROUNDUP(DISPLAY_HEIGHT * DISPLAY_PITCH, 4 * 1024), 12,
	    &db_handle[1], &db_gaddr[1], (void **) &db[1]);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("DB1 handle 0x%lx gpu addr 0x%08x\n",
	    db_handle[1], db_gaddr[1]);

	/* Allocate depth buffer */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_VIDEO,
	    ROUNDUP(DISPLAY_HEIGHT * DISPLAY_PITCH, 4 * 1024), 12,
	    &zb_handle, &zb_gaddr, (void **) &zb);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("ZB handle 0x%lx gpu addr 0x%08x\n",
	    zb_handle, zb_gaddr);

	/* Allocate fragment program */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_VIDEO,
	    4 * 1024, 12, &fp_handle, &fp_gaddr, (void **) &fp);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("FP handle 0x%lx gpu addr 0x%08x\n",
	    fp_handle, fp_gaddr);

	/* Set display buffers */

	err = display_buffer_set(fd, context_id, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT,
	    DISPLAY_PITCH, db_handle[0]);
	if (err < 0) {
		perror("display_buffer_set");
		goto done;
	}

	err = display_buffer_set(fd, context_id, 1, DISPLAY_WIDTH, DISPLAY_HEIGHT,
	    DISPLAY_PITCH, db_handle[1]);
	if (err < 0) {
		perror("display_buffer_set");
		goto done;
	}

	const struct surface_desc surf_desc[] = {
		/* display buffer 0 */
		{
			.sd_color_loc = { 0xfeed0000, 0xfeed0000, 0xfeed0000, 0xfeed0000 },
			.sd_color_off = { db_gaddr[0], 0, 0, 0 },
			.sd_color_pitch = { DISPLAY_PITCH, 64, 64, 64 },
			.sd_color_fmt = 0x8,
			.sd_color_target = 0x1,
			.sd_depth_loc = 0xfeed0000,
			.sd_depth_off = zb_gaddr,
			.sd_depth_pitch = DISPLAY_PITCH,
			.sd_depth_fmt = 0x2,
			.sd_x = 0,
			.sd_y = 0,
			.sd_w = DISPLAY_WIDTH,
			.sd_h = DISPLAY_HEIGHT,
		},
		/* display buffer 1 */
		{
			.sd_color_loc = { 0xfeed0000, 0xfeed0000, 0xfeed0000, 0xfeed0000 },
			.sd_color_off = { db_gaddr[1], 0, 0, 0 },
			.sd_color_pitch = { DISPLAY_PITCH, 64, 64, 64 },
			.sd_color_fmt = 0x8,
			.sd_color_target = 0x1,
			.sd_depth_loc = 0xfeed0000,
			.sd_depth_off = zb_gaddr,
			.sd_depth_pitch = DISPLAY_PITCH,
			.sd_depth_fmt = 0x2,
			.sd_x = 0,
			.sd_y = 0,
			.sd_w = DISPLAY_WIDTH,
			.sd_h = DISPLAY_HEIGHT,
		},
	};

	const uint32_t clear_color[] = {
		0xff404040,
		0xffffffff,
	};

	const float vp_offset[] = { DISPLAY_WIDTH * 0.5f, DISPLAY_HEIGHT * 0.5f, 0.5f, 0.0f };
	const float vp_scale[] = { DISPLAY_WIDTH * 0.5f, DISPLAY_HEIGHT * 0.5f, 0.5f, 0.0f };

	const uint32_t vertex_prg[] = {
		/* MOV o[0], v[0] */
		0x401f9c6c, 0x0040000d, 0x8106c083, 0x6041ff80,
		/* MOV o[1], v[3] */
		0x401f9c6c, 0x0040030d, 0x8106c083, 0x6041ff85,
	};

	uint32_t frag_prg[] = {
		/* MOVR R0, f[1] */
		0x3e010100, 0xc8011c9d, 0xc8000001, 0xc8003fe1,
	};

	/*
	 * (-0.5, -0.5) -------- (0.5, -0.5)
	 *             |        |
	 *             |        |
	 *             |        |
	 *             |        |
	 * (-0.5, 0.5)  -------- (0.5, 0.5)
	 *
	 */

	const float quad_pos[][4] = {
		/* xyzw */
		{  0.5f, -0.5f, -1.0f, 1.0f },
		{ -0.5f, -0.5f, -1.0f, 1.0f },
		{ -0.5f,  0.5f, -1.0f, 1.0f },
		{  0.5f,  0.5f, -1.0f, 1.0f },
	};
	const float quad_color[][4] = {
		/* rgba */
		{ 1.0f, 0.0f, 0.0f, 1.0f },
		{ 0.0f, 1.0f, 0.0f, 1.0f },
		{ 0.0f, 0.0f, 1.0f, 1.0f },
		{ 1.0f, 1.0f, 0.0f, 1.0f },
	};

	err = setup_control(fd, context_id, fifo_handle, fifo_handle, 0xdeadbabe);
	if (err < 0) {
		perror("setup_control");
		goto done;
	}

	printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
	    control[0x10], control[0x11], control[0x12]);

	/* Transfer fragment program to VRAM */

	err += transfer_inline(fifo + err, 0xfeed0000, fp_gaddr,
	    frag_prg, ARRAY_SIZE(frag_prg));

	control[0x10] = fifo_gaddr + err * sizeof(uint32_t);

	err = wait_fifo_idle(control);
	if (err < 0) {
		fprintf(stderr, "FIFO timeout: put 0x%08x get 0x%08x ref 0x%08x\n",
		    control[0x10], control[0x11], control[0x12]);
		dump_fifo(stderr, fifo, 0x400);
		goto done;
	}

	printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
	    control[0x10], control[0x11], control[0x12]);

	for (i = 0; i < ARRAY_SIZE(surf_desc); i++) {
		err = setup_control(fd, context_id, fifo_handle, fifo_handle, 0xdeadbabe);
		if (err < 0) {
			perror("setup_control");
			goto done;
		}

		printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
		    control[0x10], control[0x11], control[0x12]);

		err += set_surface(fifo + err, &surf_desc[i]);
		err += set_depth_mask(fifo + err, 0x00000000);
		err += set_color_mask(fifo + err, 0x01010101);
		err += set_color_mask_mrt(fifo + err, 0x00000000);
		err += set_clear_color(fifo + err, clear_color[i]);
		err += set_scissor(fifo + err, 0, 0, 4095, 4095);
		err += clear_surface(fifo + err, 0x000000f1);

		err += set_viewport(fifo + err, 0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT,
		    0.0f, 1.0f, vp_offset, vp_scale);

		/* Set vertex shader */

		err += load_vertex_prg(fifo + err, 0, vertex_prg, ARRAY_SIZE(vertex_prg) / 4);
		err += set_vertex_prg_start_slot(fifo + err, 0);
		err += set_vertex_prg_reg_count(fifo + err, 1);
		err += set_vertex_attr_inmask(fifo + err, (1 << 3) | (1 << 0));
		err += set_vertex_attr_outmask(fifo + err, (1 << 2) | (1 << 0));

		/* Set fragment shader */

		err += set_frag_prg(fifo + err, 0x1, fp_gaddr);
		err += frag_prg_ctrl(fifo + err, 2, 0, 0, 0, 0);

		err += set_front_poly_mode(fifo + err, 0x1b02);
		err += set_shade_mode(fifo + err, 0x1d01);

		/* register 0 - position */
		err += set_vertex_data_arrfmt(fifo + err, 0, 0, 0, 0, 2);
		/* register 3 - color */
		err += set_vertex_data_arrfmt(fifo + err, 3, 0, 0, 0, 2);

		err += draw_begin(fifo + err, 0x8);
		err += set_vertex_data_4f(fifo + err, 3, quad_color[0]);
		err += set_vertex_data_4f(fifo + err, 0, quad_pos[0]);
		err += set_vertex_data_4f(fifo + err, 3, quad_color[1]);
		err += set_vertex_data_4f(fifo + err, 0, quad_pos[1]);
		err += set_vertex_data_4f(fifo + err, 3, quad_color[2]);
		err += set_vertex_data_4f(fifo + err, 0, quad_pos[2]);
		err += set_vertex_data_4f(fifo + err, 3, quad_color[3]);
		err += set_vertex_data_4f(fifo + err, 0, quad_pos[3]);
		err += draw_end(fifo + err);

		err += flip_display_buffer(fifo + err, get_channel_id(driver_info), i, 0);

		/*
		 * Label with index 0 (head 0) is set by LV1 to 0x00000000 when flip is complete.
		 * Let GPU wait for it.
		 */

		err += wait_label(fifo + err, 0, 0x00000000);

		control[0x10] = fifo_gaddr + err * sizeof(uint32_t);

		err = wait_fifo_idle(control);
		if (err < 0) {
			fprintf(stderr, "FIFO timeout: put 0x%08x get 0x%08x ref 0x%08x\n",
			    control[0x10], control[0x11], control[0x12]);
			dump_fifo(stderr, fifo, 0x400);
			goto done;
		}

		printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
		    control[0x10], control[0x11], control[0x12]);

		usleep(1000000);
	}

	save_image("image.argb", (const char *) db[0], DISPLAY_PITCH * DISPLAY_HEIGHT);

	/* Destroy GPU context */

	context_free.context_id = context_id;

	err = ioctl(fd, PS3GPU_CTL_CONTEXT_FREE, &context_free);
	if (err < 0) {
		perror("ioctl");
		goto done;
	}

done:

	if (fd >= 0)
		close(fd);

	/* Restore console */

	ioctl(0, SW_TEXT_80x25, NULL);

	exit(0);
}
