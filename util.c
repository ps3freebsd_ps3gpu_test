/*-
 * Copyright (C) 2011, 2012 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <errno.h>

#include <sys/ioctl.h>
#include <sys/mman.h>
#include <unistd.h>

#include "ps3gpu_ctl.h"
#include "ps3gpu_mth.h"
#include "util.h"

int
setup_control(int fd, int context_id, unsigned long put, unsigned long get,
    unsigned int ref)
{
	struct ps3gpu_ctl_setup_control setup_control;

	setup_control.context_id = context_id;
	setup_control.put = put;
	setup_control.get = get;
	setup_control.ref = ref;

	return (ioctl(fd, PS3GPU_CTL_SETUP_CONTROL, &setup_control));
}

int
memory_allocate(int fd, int context_id, int type, int size, int align,
    unsigned long *handle, unsigned int *gpu_addr, void **map)
{
	struct ps3gpu_ctl_memory_allocate memory_allocate;
	struct ps3gpu_ctl_memory_free memory_free;
	int err;

	memory_allocate.context_id = context_id;
	memory_allocate.type = type;
	memory_allocate.size = size;
	memory_allocate.align = align;

	err = ioctl(fd, PS3GPU_CTL_MEMORY_ALLOCATE, &memory_allocate);
	if (err < 0)
		return (err);

	if (handle)
		*handle = memory_allocate.handle;

	if (gpu_addr)
		*gpu_addr = memory_allocate.gpu_addr;

	if (map) {
		*map = mmap(NULL, memory_allocate.size,
		    PROT_READ | PROT_WRITE, MAP_SHARED, fd, memory_allocate.handle);
		if (*map == (void *) MAP_FAILED) {
			memory_free.context_id = context_id;
			memory_free.handle = memory_allocate.handle;
			err = errno;
			ioctl(fd, PS3GPU_CTL_MEMORY_FREE, &memory_free);
			errno = err;
			return (-1);
		}
	}

	return (0);
}

int
display_buffer_set(int fd, int context_id, int buffer_id, int width, int height,
    int pitch, unsigned long offset)
{
	struct ps3gpu_ctl_display_buffer_set display_buffer_set;

	display_buffer_set.context_id = context_id;
	display_buffer_set.buffer_id = buffer_id;
	display_buffer_set.width = width;
	display_buffer_set.height = height;
	display_buffer_set.pitch = pitch;
	display_buffer_set.offset = offset;

	return (ioctl(fd, PS3GPU_CTL_DISPLAY_BUFFER_SET, &display_buffer_set));
}

int
set_flip_mode(int fd, int context_id, int head, int mode)
{
	struct ps3gpu_ctl_set_flip_mode set_flip_mode;

	set_flip_mode.context_id = context_id;
	set_flip_mode.head = head;
	set_flip_mode.mode = mode;

	return (ioctl(fd, PS3GPU_CTL_SET_FLIP_MODE, &set_flip_mode));
}

int
reset_flip_status(int fd, int context_id, int head)
{
	struct ps3gpu_ctl_reset_flip_status reset_flip_status;

	reset_flip_status.context_id = context_id;
	reset_flip_status.head = head;

	return (ioctl(fd, PS3GPU_CTL_RESET_FLIP_STATUS, &reset_flip_status));
}

int
flip(int fd, int context_id, int head, unsigned long offset)
{
	struct ps3gpu_ctl_flip flip;

	flip.context_id = context_id;
	flip.head = head;
	flip.offset = offset;

	return (ioctl(fd, PS3GPU_CTL_FLIP, &flip));
}

int
get_tiled_pitch_size(int pitch)
{
	static const int tile_pitches[] = {
		0x00000000, 0x00000200, 0x00000300, 0x00000400,
		0x00000500, 0x00000600, 0x00000700, 0x00000800,
		0x00000a00, 0x00000c00, 0x00000d00, 0x00000e00,
		0x00001000, 0x00001400, 0x00001800, 0x00001a00,
		0x00001c00, 0x00002000, 0x00002800, 0x00003000,
		0x00003400, 0x00003800, 0x00004000, 0x00005000,
		0x00006000, 0x00006800, 0x00007000, 0x00008000,
		0x0000a000, 0x0000c000, 0x0000d000, 0x0000e000,
		0x00010000,

	};
	int i;

	for (i = 0; i < ARRAY_SIZE(tile_pitches) - 1; i++) {
		if ((tile_pitches[i] < pitch) && (pitch <= tile_pitches[i + 1]))
			return (tile_pitches[i + 1]);
	}

	return (0);
}

int
tile_set(int fd, int context_id, int tile_id, int size, int pitch,
    int cmp_mode, int bank, int base, unsigned long offset)
{
	struct ps3gpu_ctl_tile_set tile_set;

	tile_set.context_id = context_id;
	tile_set.tile_id = tile_id;
	tile_set.size = size;
	tile_set.pitch = pitch;
	tile_set.cmp_mode = cmp_mode;
	tile_set.bank = bank;
	tile_set.base = base;
	tile_set.offset = offset;

	return (ioctl(fd, PS3GPU_CTL_TILE_SET, &tile_set));
}

int
tile_unset(int fd, int context_id, int tile_id, int bank,
    unsigned long offset)
{
	struct ps3gpu_ctl_tile_unset tile_unset;

	tile_unset.context_id = context_id;
	tile_unset.tile_id = tile_id;
	tile_unset.bank = bank;
	tile_unset.offset = offset;

	return (ioctl(fd, PS3GPU_CTL_TILE_UNSET, &tile_unset));
}

int
save_image(const char *filename, const char *buf, int len)
{
	FILE *fp;
	int nwritten;
	int err = 0;

	fp = fopen(filename, "w");
	if (!fp)
		return (-1);

	nwritten = fwrite(buf, 1, len, fp);
	if (nwritten != len)
		err = -1;

	fclose(fp);

	return (err);
}

void
dump_fifo(FILE *fp, const uint32_t *fifo, unsigned int word_count)
{
	int i = 0;

	while (word_count--) {
		fprintf(fp, "%08x: %08x\n", i * 4, fifo[i]);
		i++;
	}
}

int
set_reference(uint32_t *fifo, uint32_t val)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, PS3GPU_MTH_ADDR_REF);
	fifo[i++] = val;

	return (i);
}

int
transfer_data(uint32_t *fifo, uint32_t src, uint32_t dst,
    uint32_t dst_offset, int32_t dst_pitch, uint32_t src_offset, int32_t src_pitch,
    uint32_t row_length, uint32_t row_count)
{
	int i = 0;
	int h;

	fifo[i++] = PS3GPU_MTH_HDR(2, 1, 0x184);
	fifo[i++] = src;
	fifo[i++] = dst;

	while (row_count) {
		h = row_count;
		if (h > 2047)
			h = 2047;

		fifo[i++] = PS3GPU_MTH_HDR(8, 1, 0x30c);
		fifo[i++] = src_offset;
		fifo[i++] = dst_offset;
		fifo[i++] = src_pitch;
		fifo[i++] = dst_pitch;
		fifo[i++] = row_length;
		fifo[i++] = h;
		fifo[i++] = 0x00000101;
		fifo[i++] = 0x00000000;

		src_offset += h * src_pitch;
		dst_offset += h * dst_pitch;
		row_count -= h;
	}

	fifo[i++] = PS3GPU_MTH_HDR(1, 1, 0x310);
	fifo[i++] = 0x00000000;

	return (i);
}

int
transfer_inline(uint32_t *fifo, uint32_t dst, uint32_t dst_offset,
    const uint32_t *data, uint32_t word_count)
{
	int odd = word_count & 1;
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 3, 0x188);
	fifo[i++] = dst;

	fifo[i++] = PS3GPU_MTH_HDR(1, 3, 0x30c);
	fifo[i++] = dst_offset & ~0x3f;

	fifo[i++] = PS3GPU_MTH_HDR(2, 3, 0x300);
	fifo[i++] = 0x0000000b;
	fifo[i++] = 0x10001000;

	fifo[i++] = PS3GPU_MTH_HDR(3, 5, 0x304);
	fifo[i++] = (dst_offset >> 2) & 0xf;
	fifo[i++] = 0x00010000 | word_count;
	fifo[i++] = 0x00010000 | word_count;

	fifo[i++] = PS3GPU_MTH_HDR((word_count + 1) & ~1, 5, 0x400);

	while (word_count--)
		fifo[i++] = *data++;

	if (odd)
		fifo[i++] = 0x00000000;

	return (i);
}

int
flip_display_buffer(uint32_t *fifo, uint8_t channel_id, uint8_t buffer_id,
    uint8_t head)
{
	int i = 0;

	/* reset flip label */

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x64);
	fifo[i++]= (head << 4);

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x6c);
	fifo[i++] = 0x00000001;

	/* 0xcafebabe flip method */

	fifo[i++] = PS3GPU_MTH_HDR(1, 7, 0x920 + (head << 2));
	fifo[i++] = 0x80000000 | (channel_id << 8) | buffer_id;

	return (i);
}

int
set_depth_mask(uint32_t *fifo, uint32_t mask)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, PS3GPU_MTH_ADDR_DEPTH_MASK);
	fifo[i++] = mask;

	return (i);
}

int
set_color_mask(uint32_t *fifo, uint32_t mask)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, PS3GPU_MTH_ADDR_COLOR_MASK);
	fifo[i++] = mask;

	return (i);
}

int
set_color_mask_mrt(uint32_t *fifo, uint32_t mask)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, PS3GPU_MTH_ADDR_COLOR_MASK_MRT);
	fifo[i++] = mask;

	return (i);
}

int
set_clear_color(uint32_t *fifo, uint32_t color)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, PS3GPU_MTH_ADDR_CLEAR_COLOR);
	fifo[i++] = color;

	return (i);
}

int
set_scissor(uint32_t *fifo, uint16_t x, uint16_t y, uint16_t w, uint16_t h)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(2, 0, PS3GPU_MTH_ADDR_SCISSOR);
	fifo[i++] = (w << 16) | x;
	fifo[i++] = (h << 16) | y;

	return (i);
}

int
set_front_poly_mode(uint32_t *fifo, uint32_t mode)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1828);
	fifo[i++] = mode;

	return (i);
}

int
set_shade_mode(uint32_t *fifo, uint32_t mode)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x368);
	fifo[i++] = mode;

	return (i);
}

int
blend_enable(uint32_t *fifo, uint32_t enable)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x310);
	fifo[i++] = enable;

	return (i);
}

int
clear_surface(uint32_t *fifo, uint32_t mask)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1d94);
	fifo[i++] = mask;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x100);
	fifo[i++] = 0x00000000;

	return (i);
}

int
set_surface(uint32_t *fifo, const struct surface_desc *sd)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x194);
	fifo[i++] = sd->sd_color_loc[0];

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x18c);
	fifo[i++] = sd->sd_color_loc[1];

	fifo[i++] = PS3GPU_MTH_HDR(2, 0, 0x1b4);
	fifo[i++] = sd->sd_color_loc[2];
	fifo[i++] = sd->sd_color_loc[3];

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x198);
	fifo[i++] = sd->sd_depth_loc;

	fifo[i++] = PS3GPU_MTH_HDR(6, 0, 0x208);
	fifo[i++] = ((uint8_t) log2(sd->sd_h) << 24) | ((uint8_t) log2(sd->sd_w) << 16) |
	    (0x0 << 12) | (0x1 << 8) |
	    (sd->sd_depth_fmt << 5) | sd->sd_color_fmt;
	fifo[i++] = sd->sd_color_pitch[0];
	fifo[i++] = sd->sd_color_off[0];
	fifo[i++] = sd->sd_depth_off;
	fifo[i++] = sd->sd_color_off[1];
	fifo[i++] = sd->sd_color_pitch[1];

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x22c);
	fifo[i++] = sd->sd_depth_pitch;

	fifo[i++] = PS3GPU_MTH_HDR(4, 0, 0x280);
	fifo[i++] = sd->sd_color_pitch[2];
	fifo[i++] = sd->sd_color_pitch[3];
	fifo[i++] = sd->sd_color_off[2];
	fifo[i++] = sd->sd_color_off[3];

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x220);
	fifo[i++] = sd->sd_color_target;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x2b8);
	fifo[i++] = (sd->sd_y << 16) | sd->sd_x;

	fifo[i++] = PS3GPU_MTH_HDR(2, 0, 0x200);
	fifo[i++] = (sd->sd_w << 16) | sd->sd_x;
	fifo[i++] = (sd->sd_h << 16) | sd->sd_y;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1d88);
	fifo[i++] = (0x0 << 16) | (0x1 << 12) |
	    (sd->sd_h - ((sd->sd_h >> 12) & 0x1));

	return (i);
}

int
wait_label(uint32_t *fifo, uint32_t index, uint32_t val)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x64);
	fifo[i++] = index << 4;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x68);
	fifo[i++] = val;

	return (i);
}

int
write_label(uint32_t *fifo, uint32_t index, uint32_t val)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x64);
	fifo[i++] = index << 4;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x6c);
	fifo[i++] = val;

	return (i);
}

int
write_backend_label(uint32_t *fifo, uint32_t index, uint32_t val)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1d6c);
	fifo[i++] = index << 4;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1d70);
	fifo[i++] = (val & 0xff00ff00) | ((val & 0xff) << 16) | ((val >> 16) & 0xff);

	return (i);
}

int
write_texture_label(uint32_t *fifo, uint32_t index, uint32_t val)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1d6c);
	fifo[i++] = index << 4;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1d74);
	fifo[i++] = val;

	return (i);
}

int
set_viewport(uint32_t *fifo, uint16_t x, uint16_t y, uint16_t w, uint16_t h,
    float zmin, float zmax, const float offset[4], const float scale[4])
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(2, 0, 0xa00);
	fifo[i++] = (w << 16) | x;
	fifo[i++] = (h << 16) | y;

	fifo[i++] = PS3GPU_MTH_HDR(2, 0, 0x394);
	fifo[i++] = *(uint32_t *) &zmin;
	fifo[i++] = *(uint32_t *) &zmax;

	fifo[i++] = PS3GPU_MTH_HDR(8, 0, 0xa20);
	fifo[i++] = *(uint32_t *) &offset[0];
	fifo[i++] = *(uint32_t *) &offset[1];
	fifo[i++] = *(uint32_t *) &offset[2];
	fifo[i++] = *(uint32_t *) &offset[3];
	fifo[i++] = *(uint32_t *) &scale[0];
	fifo[i++] = *(uint32_t *) &scale[1];
	fifo[i++] = *(uint32_t *) &scale[2];
	fifo[i++] = *(uint32_t *) &scale[3];

	fifo[i++] = PS3GPU_MTH_HDR(8, 0, 0xa20);
	fifo[i++] = *(uint32_t *) &offset[0];
	fifo[i++] = *(uint32_t *) &offset[1];
	fifo[i++] = *(uint32_t *) &offset[2];
	fifo[i++] = *(uint32_t *) &offset[3];
	fifo[i++] = *(uint32_t *) &scale[0];
	fifo[i++] = *(uint32_t *) &scale[1];
	fifo[i++] = *(uint32_t *) &scale[2];
	fifo[i++] = *(uint32_t *) &scale[3];

	return (i);
}

int
load_vertex_prg(uint32_t *fifo, uint16_t slot, const uint32_t *prg, int instr_count)
{
	int i = 0;
	int j;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1e9c);
	fifo[i++] = slot;

	while (instr_count >= 32) {
		fifo[i++] = PS3GPU_MTH_HDR(32, 0, 0xb80);

		for (j = 0; j < 32; j++) {
			fifo[i++] = *prg++;
			fifo[i++] = *prg++;
			fifo[i++] = *prg++;
			fifo[i++] = *prg++;
		}

		instr_count -= 32;
	}

	if (instr_count > 0) {
		fifo[i++] = PS3GPU_MTH_HDR(instr_count << 2, 0, 0xb80);

		for (j = 0; j < instr_count; j++) {
			fifo[i++] = *prg++;
			fifo[i++] = *prg++;
			fifo[i++] = *prg++;
			fifo[i++] = *prg++;
		}
	}

	return (i);
}

int
set_vertex_prg_reg_count(uint32_t *fifo, unsigned int count)
{
	int i = 0;
	uint32_t val;

	val = (count <= 32) ? 0x0020ffff : 0x0030ffff;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1ef8);
	fifo[i++] = val;

	return (i);
}

int
set_vertex_prg_start_slot(uint32_t *fifo, uint16_t slot)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1ea0);
	fifo[i++] = slot;

	return (i);
}

int
set_vertex_prg_const(uint32_t *fifo, uint32_t start, uint32_t count,
    const float *val)
{
	int i = 0;
	int j;

	while (count >= 32) {
		fifo[i++] = PS3GPU_MTH_HDR(33, 0, 0x1efc);
		fifo[i++] = start;

		for (j = 0; j < 32; j++)
			fifo[i++] = *(uint32_t *) val++;

		count -= 32;
		start += 8;
	}

	if (count > 0) {
		fifo[i++] = PS3GPU_MTH_HDR(count + 1, 0, 0x1efc);
		fifo[i++] = start;

		for (j = 0; j < count; j++)
			fifo[i++] = *(uint32_t *) val++;
	}

	return (i);
}

int
set_vertex_attr_inmask(uint32_t *fifo, uint32_t mask)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1ff0);
	fifo[i++] = mask;

	return (i);
}

int
set_vertex_attr_outmask(uint32_t *fifo, uint32_t mask)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1ff4);
	fifo[i++] = mask;

	return (i);
}

int
set_frag_prg(uint32_t *fifo, uint8_t location, uint32_t offset)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x8e4);
	fifo[i++] = offset | location;

	return (i);
}

int
frag_prg_ctrl(uint32_t *fifo, uint8_t reg_count, uint8_t replace_txp_with_tex,
    uint8_t pixel_kill, uint8_t output_from_h0, uint8_t depth_replace)
{
	int i = 0;

	if (reg_count <= 1)
		reg_count = 2;

	if (depth_replace)
		depth_replace = 0x7;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1d60);
	fifo[i++] = (reg_count << 24) | (replace_txp_with_tex << 15) | 0x00000400 |
	    (pixel_kill << 7) | (!output_from_h0 << 6) | (depth_replace << 1);

	return (i);
}

int
draw_begin(uint32_t *fifo, uint32_t mode)
{
	int i = 0;

	fifo[i++] = 0x40000000 | PS3GPU_MTH_HDR(3, 0, 0x1714);
	fifo[i++] = 0x00000000;
	fifo[i++] = 0x00000000;
	fifo[i++] = 0x00000000;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1808);
	fifo[i++] = mode;

	return (i);
}

int
draw_end(uint32_t *fifo)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1808);
	fifo[i++] = 0x00000000;

	return (i);
}

int
set_vertex_data_arrfmt(uint32_t *fifo, uint8_t reg, uint16_t freq,
    uint8_t stride, uint8_t size, uint8_t type)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1740 + (reg << 4));
	fifo[i++] = (freq << 16) | (stride << 8) | (size << 4) | type;

	return (i);
}

int
set_vertex_data_2f(uint32_t *fifo, uint8_t reg, const float val[2])
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(2, 0, 0x1880 + (reg << 3));
	fifo[i++] = *(uint32_t *) &val[0];
	fifo[i++] = *(uint32_t *) &val[1];

	return (i);
}

int
set_vertex_data_4f(uint32_t *fifo, uint8_t reg, const float val[4])
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(4, 0, 0x1c00 + (reg << 4));
	fifo[i++] = *(uint32_t *) &val[0];
	fifo[i++] = *(uint32_t *) &val[1];
	fifo[i++] = *(uint32_t *) &val[2];
	fifo[i++] = *(uint32_t *) &val[3];

	return (i);
}

int
set_vertex_data_arr(uint32_t *fifo, uint8_t reg,
    uint16_t freq, uint8_t stride, uint8_t size, uint8_t type,
    uint8_t location, uint32_t offset)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1740 + (reg << 2));
	fifo[i++] = (freq << 16) | (stride << 8) | (size << 4) | type;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1680 + (reg << 2));
	fifo[i++] = (location << 31) | offset;

	return (i);
}

int
draw_arrays(uint32_t *fifo, uint32_t mode, uint32_t first, uint32_t count)
{
	int i = 0;
	int n, m, j;

	/* draw begin */

	fifo[i++] = 0x40000000 | PS3GPU_MTH_HDR(3, 0, 0x1714);
	fifo[i++] = 0x00000000;
	fifo[i++] = 0x00000000;
	fifo[i++] = 0x00000000;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1808);
	fifo[i++] = mode;

	while (count > 0) {
		n = (count + 0x100 - 1) / 0x100;
		if (n > 0x7ff)
			n = 0x7ff;

		fifo[i++] = 0x40000000 | PS3GPU_MTH_HDR(n, 0, 0x1814);

		for (j = 0; j < n; j++) {
			m = count;
			if (m > 0x100)
				m = 0x100;

			fifo[i++] = ((m - 1) << 24) | first;

			first += m;
			count -= m;
		}
	}

	/* draw end */

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1808);
	fifo[i++] = 0x00000000;

	return (i);
}

int
invalidate_vertex_cache(uint32_t *fifo)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1710);
	fifo[i++] = 0x00000000;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1714);
	fifo[i++] = 0x00000000;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1714);
	fifo[i++] = 0x00000000;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1714);
	fifo[i++] = 0x00000000;

	return (i);
}

int
set_texture(uint32_t *fifo, uint8_t index, const struct texture_desc *td)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(2, 0, 0x1a00 + (index << 5));
	fifo[i++] = td->td_off;
	fifo[i++] = (td->td_mipmap << 16) | (td->td_fmt << 8) |
	    (td->td_dimension << 4) | (td->td_border << 3) |
	    (td->td_cubemap << 2) | td->td_loc;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1a18 + (index << 5));
	fifo[i++] = (td->td_w << 16) | td->td_h;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1840 + (index << 2));
	fifo[i++] = (td->td_depth << 20) | td->td_pitch;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1a10 + (index << 5));
	fifo[i++] = td->td_remap;

	return (i);
}

int
texture_ctrl(uint32_t *fifo, uint8_t index, uint8_t enable,
    uint16_t min_lod, uint16_t max_lod, uint8_t max_aniso)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1a0c + (index << 5));
	fifo[i++] = (enable << 31) | ((min_lod & 0xfff) << 19) |
	    ((max_lod & 0xfff) << 7) | (max_aniso << 4);

	return (i);
}

int
set_texture_addr(uint32_t *fifo, uint8_t index,
    uint8_t wrap_s, uint8_t wrap_t, uint8_t wrap_r, uint8_t unsigned_remap,
    uint8_t zfunc, uint8_t gamma)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1a08 + (index << 5));
	fifo[i++] = (zfunc << 28) | (gamma << 20) | (wrap_r << 16) |
	    (unsigned_remap << 12) | (wrap_t << 8) | wrap_s;

	return (i);
}

int
set_texture_filter(uint32_t *fifo, uint8_t index,
    uint16_t bias, uint8_t min, uint8_t mag, uint8_t conv)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1a14 + (index << 5));
	fifo[i++] = (mag << 24) | (min << 16) | (conv << 13) | (bias & 0x1fff);

	return (i);
}

int
invalidate_texture_cache(uint32_t *fifo, uint32_t val)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1fd8);
	fifo[i++] = val;

	return (i);
}

int
set_timestamp(uint32_t *fifo, uint32_t index)
{
	int i = 0;

	fifo[i++] = PS3GPU_MTH_HDR(1, 0, 0x1800);
	fifo[i++] = 0x01000000 | index << 4;

	return (i);
}
