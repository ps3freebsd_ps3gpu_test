/*-
 * Copyright (C) 2011 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#ifndef _PS3GPU_MTH_H
#define _PS3GPU_MTH_H

#define PS3GPU_MTH_HDR(_ds, _sch, _ma)			(((_ds) << 18) | ((_sch) << 13) | (_ma))

#define PS3GPU_MTH_ADDR_NOP				0x00000000

#define PS3GPU_MTH_ADDR_CALL				0x00000002
#define PS3GPU_MTH_ADDR_RET				0x00020000
#define PS3GPU_MTH_ADDR_JMP				0x20000000

#define PS3GPU_MTH_ADDR_REF				0x00000050

#define PS3GPU_MTH_ADDR_COLOR_MASK			0x00000324
#define PS3GPU_MTH_ADDR_COLOR_MASK_MRT			0x00000370

#define PS3GPU_MTH_ADDR_SCISSOR				0x000008c0

#define PS3GPU_MTH_ADDR_DEPTH_MASK			0x00000a70

#define PS3GPU_MTH_ADDR_CLEAR_COLOR			0x00001d90

#endif /* _PS3GPU_MTH_H */
