/*-
 * Copyright (C) 2011, 2012 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/fbio.h>
#include <sys/consio.h>
#include <fcntl.h>
#include <unistd.h>

#include "ps3gpu_ctl.h"
#include "ps3gpu_mth.h"
#include "util.h"

static const uint64_t cursor_bitmap[64] = {
	0x000000ffff000000,
	0x0000ff0000ff0000,
	0x00ff00000000ff00,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0xff000000000000ff,
	0x00ff00000000ff00,
	0x0000ff0000ff0000,
	0x000000ffff000000,
};


static int
update_cursor(int fd, int context_id, int head, unsigned long offset,
    int x, int y, int enable)
{
	struct ps3gpu_ctl_cursor_enable cursor_enable;
	struct ps3gpu_ctl_cursor_set_image cursor_set_image;
	struct ps3gpu_ctl_cursor_set_position cursor_set_position;
	int err;

	/* Disable cursor */

	cursor_enable.context_id = context_id;
	cursor_enable.head = head;
	cursor_enable.enable = 0;

	err = ioctl(fd, PS3GPU_CTL_CURSOR_ENABLE, &cursor_enable);
	if (err < 0) {
		perror("ioctl");
		return (err);
	}

	/* Set cursor image */

	cursor_set_image.context_id = context_id;
	cursor_set_image.head = head;
	cursor_set_image.offset = offset;

	err = ioctl(fd, PS3GPU_CTL_CURSOR_SET_IMAGE, &cursor_set_image);
	if (err < 0) {
		perror("ioctl");
		return (err);
	}

	/* Set cursor position */

	cursor_set_position.context_id = context_id;
	cursor_set_position.head = head;
	cursor_set_position.x = x;
	cursor_set_position.y = y;

	err = ioctl(fd, PS3GPU_CTL_CURSOR_SET_POSITION, &cursor_set_position);
	if (err < 0) {
		perror("ioctl");
		return (err);
	}

	/* Enable cursor */

	cursor_enable.context_id = context_id;
	cursor_enable.head = head;
	cursor_enable.enable = enable;

	err = ioctl(fd, PS3GPU_CTL_CURSOR_ENABLE, &cursor_enable);
	if (err < 0) {
		perror("ioctl");
		return (err);
	}

	return (0);
}

int
main(int argc, char **argv)
{
	struct ps3gpu_ctl_context_allocate context_allocate;
	struct ps3gpu_ctl_context_free context_free;
	int context_id;
	volatile uint32_t *control;
	uint32_t *vram, *cursor;
	unsigned long vram_handle, cursor_handle;
	unsigned int vram_gaddr, cursor_gaddr;
	int fd = -1;
	int x, y;
	int err;

	/* Open GPU device */

	fd = open(PS3GPU_DEV_PATH, O_RDWR);
	if (fd < 0) {
		perror("open");
		goto done;
	}

	/* Create GPU context */

	context_allocate.vram_size = 64; /* MB */

	err = ioctl(fd, PS3GPU_CTL_CONTEXT_ALLOCATE, &context_allocate);
	if (err < 0) {
		perror("ioctl");
		goto done;
	}

	context_id = context_allocate.context_id;

	printf("context id %d\n", context_id);
	printf("control handle 0x%lx size %d\n",
	    context_allocate.control_handle, context_allocate.control_size);

	/* Map control registers */

	control = mmap(NULL, context_allocate.control_size,
	    PROT_READ | PROT_WRITE, MAP_SHARED, fd, context_allocate.control_handle);
	if (control == (void *) MAP_FAILED) {
		perror("mmap");
		goto done;
	}

	/* Allocate VRAM */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_VIDEO,
	    ROUNDUP(DISPLAY_HEIGHT * DISPLAY_PITCH, 4 * 1024), 12,
	    &vram_handle, &vram_gaddr, (void **) &vram);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("VRAM handle 0x%lx gpu addr 0x%08x\n",
	    vram_handle, vram_gaddr);

	memset32(vram, 0xff404040, DISPLAY_HEIGHT * DISPLAY_WIDTH);

	/* Allocate cursor */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_VIDEO,
	    16 * 1024, 20, &cursor_handle, &cursor_gaddr, (void **) &cursor);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("cursor handle 0x%lx gpu addr 0x%08x\n",
	    cursor_handle, cursor_gaddr);

	/* Create cursor image */

	for (y = 0; y < 64; y++) {
		for (x = 0; x < 64; x++)
			if (cursor_bitmap[(y * 64 + x) >> 6] &
			        (1ul << ((63 - (y * 64 + x)) & 0x3f)))
				cursor[y * 64 + x] = 0xffffff00;
			else
				cursor[y * 64 + x] = 0x00000000;
	}

	/* Update cursor */

	err = update_cursor(fd, context_id, PS3GPU_CTL_HEAD_A,
	    cursor_handle, 100, 100, 1);
	if (err)
		goto done;

	err = update_cursor(fd, context_id, PS3GPU_CTL_HEAD_B,
	    cursor_handle, 100, 100, 1);
	if (err)
		goto done;

	/* Flip */

	err = flip(fd, context_id, PS3GPU_CTL_HEAD_A, vram_handle);
	if (err < 0) {
		perror("flip");
		goto done;
	}

	err = flip(fd, context_id, PS3GPU_CTL_HEAD_B, vram_handle);
	if (err < 0) {
		perror("flip");
		goto done;
	}

	usleep(2000000);

	/* Update cursor */

	err = update_cursor(fd, context_id, PS3GPU_CTL_HEAD_A,
	    cursor_handle, 300, 300, 1);
	if (err) {
		fprintf(stderr, "could not update cursor\n");
		goto done;
	}

	err = update_cursor(fd, context_id, PS3GPU_CTL_HEAD_B,
	    cursor_handle, 300, 300, 1);
	if (err) {
		fprintf(stderr, "could not update cursor\n");
		goto done;
	}

	usleep(2000000);

	/* Destroy GPU context */

	context_free.context_id = context_id;

	err = ioctl(fd, PS3GPU_CTL_CONTEXT_FREE, &context_free);
	if (err < 0) {
		perror("ioctl");
		goto done;
	}

done:

	if (fd >= 0)
		close(fd);

	/* Restore console */

	ioctl(0, SW_TEXT_80x25, NULL);

	exit(0);
}
