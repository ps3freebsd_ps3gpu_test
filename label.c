/*-
 * Copyright (C) 2011, 2012 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/fbio.h>
#include <sys/consio.h>
#include <fcntl.h>
#include <unistd.h>

#include "ps3gpu_ctl.h"
#include "ps3gpu_mth.h"
#include "reset_gpu_state.h"
#include "util.h"

int
main(int argc, char **argv)
{
	struct ps3gpu_ctl_context_allocate context_allocate;
	struct ps3gpu_ctl_context_free context_free;
	int context_id;
	volatile uint32_t *control;
	volatile uint8_t *driver_info;
	volatile uint8_t *reports;
	uint32_t *fifo, *reset_gpu;
	unsigned long fifo_handle, reset_gpu_handle;
	unsigned int fifo_gaddr, reset_gpu_gaddr;
	uint32_t label_index;
	int fd = -1;
	int err;

	/* Open GPU device */

	fd = open(PS3GPU_DEV_PATH, O_RDWR);
	if (fd < 0) {
		perror("open");
		goto done;
	}

	/* Create GPU context */

	context_allocate.vram_size = 64; /* MB */

	err = ioctl(fd, PS3GPU_CTL_CONTEXT_ALLOCATE, &context_allocate);
	if (err < 0) {
		perror("ioctl");
		goto done;
	}

	context_id = context_allocate.context_id;

	printf("context id %d\n", context_id);
	printf("control handle 0x%lx size %d\n",
	    context_allocate.control_handle, context_allocate.control_size);
	printf("driver_info handle 0x%lx size %d\n",
	    context_allocate.driver_info_handle, context_allocate.driver_info_size);
	printf("reports handle 0x%lx size %d\n",
	    context_allocate.reports_handle, context_allocate.reports_size);

	/* Map control registers */

	control = mmap(NULL, context_allocate.control_size,
	    PROT_READ | PROT_WRITE, MAP_SHARED, fd, context_allocate.control_handle);
	if (control == (void *) MAP_FAILED) {
		perror("mmap");
		goto done;
	}

	/* Map driver info */

	driver_info = mmap(NULL, context_allocate.driver_info_size,
	    PROT_READ | PROT_WRITE, MAP_SHARED, fd, context_allocate.driver_info_handle);
	if (driver_info == (void *) MAP_FAILED) {
		perror("mmap");
		goto done;
	}

	/* Map reports */

	reports = mmap(NULL, context_allocate.reports_size,
	    PROT_READ | PROT_WRITE, MAP_SHARED, fd, context_allocate.reports_handle);
	if (reports == (void *) MAP_FAILED) {
		perror("mmap");
		goto done;
	}

	printf("channel id %d\n", get_channel_id(driver_info));
	printf("label area offset 0x%08x\n", get_label_area_offset(driver_info));
	printf("report data area offset 0x%08x\n", get_report_data_area_offset(driver_info));

	/* Allocate FIFO */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_GART,
	    64 * 1024, 12, &fifo_handle, &fifo_gaddr, (void **) &fifo);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("FIFO handle 0x%lx gpu addr 0x%08x\n",
	    fifo_handle, fifo_gaddr);

	/* Setup FIFO */
	
	err = setup_control(fd, context_id, fifo_handle, fifo_handle, 0xdeadbabe);
	if (err < 0) {
		perror("setup_control");
		goto done;
	}

	printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
	    control[0x10], control[0x11], control[0x12]);

	/* Allocate FIFO for resetting GPU state */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_GART,
	    4 * 1024, 12, &reset_gpu_handle, &reset_gpu_gaddr, (void **)&reset_gpu);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("reset GPU state handle 0x%lx gpu addr 0x%08x\n",
	    reset_gpu_handle, reset_gpu_gaddr);

	memcpy(reset_gpu, reset_gpu_state_3d, reset_gpu_state_3d_size);

	/* Kick FIFO */

	fifo[0] = PS3GPU_MTH_HDR(0, 0, reset_gpu_gaddr | PS3GPU_MTH_ADDR_CALL);
	fifo[1] = PS3GPU_MTH_HDR(1, 0, PS3GPU_MTH_ADDR_REF);
	fifo[2] = 0xcafef00d;

	control[0x10] = fifo_gaddr + 3 * sizeof(uint32_t);

	err = wait_fifo_idle(control);
	if (err < 0) {
		fprintf(stderr, "FIFO timeout: put 0x%08x get 0x%08x ref 0x%08x\n",
		    control[0x10], control[0x11], control[0x12]);
		dump_fifo(stderr, fifo, 0x400);
		goto done;
	}

	printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
	    control[0x10], control[0x11], control[0x12]);

	/* Test label */

	err = setup_control(fd, context_id, fifo_handle, fifo_handle, 0xdeadbabe);
	if (err < 0) {
		perror("setup_control");
		goto done;
	}

	printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
	    control[0x10], control[0x11], control[0x12]);

	label_index = 128;

	/* Reset label */

	*get_label_addr(driver_info, reports, label_index) = 0;
	*get_label_addr(driver_info, reports, label_index + 1) = 0;
	*get_label_addr(driver_info, reports, label_index + 2) = 0;

	printf("label #%d value 0x%08x\n", label_index,
	    get_label_value(driver_info, reports, label_index));
	printf("label #%d value 0x%08x\n", label_index + 1,
	    get_label_value(driver_info, reports, label_index + 1));
	printf("label #%d value 0x%08x\n", label_index + 2,
	    get_label_value(driver_info, reports, label_index + 2));

	/* Write label */

	err += write_label(fifo + err, label_index, 0xcafebabe);
	err += write_backend_label(fifo + err, label_index + 1, 0xb00bf00d);
	err += write_texture_label(fifo + err, label_index + 2, 0xdeadbeef);

	control[0x10] = fifo_gaddr + err * sizeof(uint32_t);

	err = wait_fifo_idle(control);
	if (err < 0) {
		fprintf(stderr, "FIFO timeout: put 0x%08x get 0x%08x ref 0x%08x\n",
		    control[0x10], control[0x11], control[0x12]);
		dump_fifo(stderr, fifo, 0x400);
		goto done;
	}

	printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
	    control[0x10], control[0x11], control[0x12]);

	printf("label #%d value 0x%08x\n", label_index,
	    get_label_value(driver_info, reports, label_index));
	printf("label #%d value 0x%08x\n", label_index + 1,
	    get_label_value(driver_info, reports, label_index + 1));
	printf("label #%d value 0x%08x\n", label_index + 2,
	    get_label_value(driver_info, reports, label_index + 2));

	/* Destroy GPU context */

	context_free.context_id = context_id;

	err = ioctl(fd, PS3GPU_CTL_CONTEXT_FREE, &context_free);
	if (err < 0) {
		perror("ioctl");
		goto done;
	}

done:

	if (fd >= 0)
		close(fd);

	/* Restore console */

	ioctl(0, SW_TEXT_80x25, NULL);

	exit(0);
}
