/*-
 * Copyright (C) 2011, 2012 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#ifndef _UTIL_H
#define _UTIL_H

#define ARRAY_SIZE(_a)		(sizeof(_a) / sizeof((_a)[0]))
#define ROUNDUP(_x, _y)		((((_x) + ((_y) - 1)) / (_y)) * (_y))

struct surface_desc {
	uint32_t sd_color_loc[4];
	uint32_t sd_color_off[4];
	uint32_t sd_color_pitch[4];
	uint8_t sd_color_fmt;
	uint8_t sd_color_target;
	uint32_t sd_depth_loc;
	uint32_t sd_depth_off;
	uint32_t sd_depth_pitch;
	uint8_t sd_depth_fmt;
	uint16_t sd_x;
	uint16_t sd_y;
	uint16_t sd_w;
	uint16_t sd_h;
};

struct texture_desc {
	uint8_t td_loc;
	uint32_t td_off;
	uint32_t td_pitch;
	uint16_t td_w;
	uint16_t td_h;
	uint8_t td_fmt;
	uint8_t td_border;
	uint16_t td_depth;
	uint8_t td_dimension;
	uint8_t td_mipmap;
	uint8_t td_cubemap;
	uint32_t td_remap;
};

static inline void
memset32(void *buf, uint32_t val, int word_count)
{
	uint32_t *ptr = buf;

	while (word_count--)
		*ptr++ = val;
}

static inline uint32_t
get_channel_id(const volatile void *driver_info)
{
	return (*(uint32_t *) ((uint8_t *) driver_info + 0xc));
}

static inline uint32_t
get_label_area_offset(const volatile void *driver_info)
{
	return (*(uint32_t *) ((uint8_t *) driver_info + 0x30));
}

static inline uint32_t
get_report_data_area_offset(const volatile void *driver_info)
{
	return (*(uint32_t *) ((uint8_t *) driver_info + 0x34));
}

static inline int
get_flip_status(const volatile void *driver_info, uint8_t head)
{
	uint32_t val;

	val =  *(uint32_t *) ((uint8_t *) driver_info + 0x10c0 + head * 0x40);

	return !(val >> 31);
}

static inline int
wait_fifo_idle(volatile uint32_t *control)
{
	int count = 10000;

	while (count--) {
		if (control[0x10] == control[0x11])
			break;

		usleep(100);
	}

	if (control[0x10] != control[0x11])
		return (-1);

	return (0);
}

static inline uint32_t *
get_label_addr(const volatile void *driver_info, const volatile void *reports,
    uint32_t index)
{
	return ((uint32_t *) ((uint8_t *) reports +
	    get_label_area_offset(driver_info) + (index << 4)));
}

static inline uint32_t
get_label_value(const volatile void *driver_info, const volatile void *reports,
    uint32_t index)
{
	uint32_t val;

	val =  *get_label_addr(driver_info, reports, index);

	return (val);
}

static inline uint64_t *
get_timestamp_addr(const volatile void *driver_info, const volatile void *reports,
    uint32_t index)
{
	return ((uint64_t *) ((uint8_t *) reports +
	    get_report_data_area_offset(driver_info) + (index << 4)));
}

static inline uint64_t
get_timestamp_value(const volatile void *driver_info, const volatile void *reports,
    uint32_t index)
{
	uint64_t val;

	val =  *get_timestamp_addr(driver_info, reports, index);

	return (val);
}

int setup_control(int fd, int context_id, unsigned long put, unsigned long get,
    unsigned int ref);

int memory_allocate(int fd, int context_id, int type, int size, int align,
    unsigned long *handle, unsigned int *gpu_addr, void **map);

int display_buffer_set(int fd, int context_id, int buffer_id, int width, int height,
    int pitch, unsigned long offset);

int set_flip_mode(int fd, int context_id, int head, int mode);

int reset_flip_status(int fd, int context_id, int head);

int flip(int fd, int context_id, int head, unsigned long offset);

int get_tiled_pitch_size(int pitch);

int tile_set(int fd, int context_id, int tile_id, int size, int pitch,
    int cmp_mode, int bank, int base, unsigned long offset);

int tile_unset(int fd, int context_id, int tile_id, int bank,
    unsigned long offset);

/*
 * How to convert image to JPG format:
 *
 * convert -depth 8 -size 1920x1080 rgba:image.argb \
 * 	-channel All -separate -clone 0 -delete 0 -combine -channel BA \
 * 	-negate +channel -background white -flatten image.jpg
 *
 * convert image.jpg -resize 640x480 image_small.jpg
 */
int save_image(const char *filename, const char *buf, int len);

void dump_fifo(FILE *fp, const uint32_t *fifo, unsigned int word_count);

int set_reference(uint32_t *fifo, uint32_t val);

int transfer_data(uint32_t *fifo, uint32_t src, uint32_t dst,
    uint32_t dst_offset, int32_t dst_pitch,
    uint32_t src_offset, int32_t src_pitch,
    uint32_t row_length, uint32_t row_count);

int transfer_inline(uint32_t *fifo, uint32_t dst, uint32_t dst_offset,
    const uint32_t *data, uint32_t word_count);

int flip_display_buffer(uint32_t *fifo, uint8_t channel_id, uint8_t buffer_id,
    uint8_t head);

int set_depth_mask(uint32_t *fifo, uint32_t mask);

int set_color_mask(uint32_t *fifo, uint32_t mask);

int set_color_mask_mrt(uint32_t *fifo, uint32_t mask);

int set_clear_color(uint32_t *fifo, uint32_t color);

int set_scissor(uint32_t *fifo, uint16_t x, uint16_t y, uint16_t w, uint16_t h);

int set_front_poly_mode(uint32_t *fifo, uint32_t mode);

int set_shade_mode(uint32_t *fifo, uint32_t mode);

int blend_enable(uint32_t *fifo, uint32_t enable);

int clear_surface(uint32_t *fifo, uint32_t mask);

int set_surface(uint32_t *fifo, const struct surface_desc *sd);

int wait_label(uint32_t *fifo, uint32_t index, uint32_t val);

int write_label(uint32_t *fifo, uint32_t index, uint32_t val);

int write_backend_label(uint32_t *fifo, uint32_t index, uint32_t val);

int write_texture_label(uint32_t *fifo, uint32_t index, uint32_t val);

int set_viewport(uint32_t *fifo, uint16_t x, uint16_t y, uint16_t w, uint16_t h,
    float zmin, float zmax, const float offset[4], const float scale[4]);

int load_vertex_prg(uint32_t *fifo, uint16_t slot, const uint32_t *prg, int instr_count);

int set_vertex_prg_reg_count(uint32_t *fifo, unsigned int count);

int set_vertex_prg_start_slot(uint32_t *fifo, uint16_t slot);

int set_vertex_prg_const(uint32_t *fifo, uint32_t start, uint32_t count,
    const float *val);

/*
 *  0 ATTR0, POSITION Vertex (float4)
 *  1 ATTR1, BLENDWEIGHT Vertex weight (float)
 *  2 ATTR2, NORMAL Normal (float3)
 *  3 ATTR3, COLOR, COLOR0, DIFFUSE Color (float4)
 *  4 ATTR4, COLOR1, SPECULAR Secondary color (float4)
 *  5 ATTR5, FOGCOORD, TESSFACTOR Fog coordinate (float)
 *  6 ATTR6, PSIZE Point size (float)
 *  7 ATTR7, BLENDINDICES Palette index for skinning (float4)
 *  8 ATTR8, TEXCOORD0 Texture coordinate 0 (float4)
 *  9 ATTR9, TEXCOORD1 Texture coordinate 1 (float4)
 * 10 ATTR10, TEXCOORD2 Texture coordinate 2 (float4)
 * 11 ATTR11, TEXCOORD3 Texture coordinate 3 (float4)
 * 12 ATTR12, TEXCOORD4 Texture coordinate 4 (float4)
 * 13 ATTR13, TEXCOORD5 Texture coordinate 5 (float4)
 * 14 ATTR14, TEXCOORD6, TANGENT Texture coordinate 6 (float4) Tangent vector (float4)
 * 15 ATTR15, TEXCOORD7, BINORMAL Texture coordinate 7 (float4) Binormal vector (float4)
 */
int set_vertex_attr_inmask(uint32_t *fifo, uint32_t mask);

/*
 *  0 COLOR, COLOR0, COL0 Front diffuse color
 *  1 COLOR1, COL1 Front specular color
 *  2 BCOL0 Back diffuse color
 *  3 BCOL1 Back specular color
 *  4 FOG, FOGC Fog
 *  5 PSIZE, PSIZ Point size
 *  6 CLP0 User clip plane 0
 *  7 CLP1 User clip plane 1
 *  8 CLP2 User clip plane 2 
 *  9 CLP3 User clip plane 3
 * 10 CLP4 User clip plane 4
 * 11 CLP5 User clip plane 5
 * 12 TEXCOORD8, TEX8 Texture coordinate 8
 * 13 TEXCOORD9, TEX9 Texture coordinate 9
 * 14 TEXCOORD0, TEX0 Texture coordinate 0
 * 15 TEXCOORD1, TEX1 Texture coordinate 1
 * 16 TEXCOORD2, TEX2 Texture coordinate 2
 * 17 TEXCOORD3, TEX3 Texture coordinate 3
 * 18 TEXCOORD4, TEX4 Texture coordinate 4
 * 19 TEXCOORD5, TEX5 Texture coordinate 5
 * 20 TEXCOORD6, TEX6 Texture coordinate 6
 * 21 TEXCOORD7, TEX7 Texture coordinate 7
 */
int set_vertex_attr_outmask(uint32_t *fifo, uint32_t mask);

int set_frag_prg(uint32_t *fifo, uint8_t location, uint32_t offset);

int frag_prg_ctrl(uint32_t *fifo, uint8_t reg_count, uint8_t replace_txp_with_tex,
    uint8_t pixel_kill, uint8_t output_from_h0, uint8_t depth_replace);

int draw_begin(uint32_t *fifo, uint32_t mode);

int draw_end(uint32_t *fifo);

int set_vertex_data_arrfmt(uint32_t *fifo, uint8_t reg, uint16_t freq,
    uint8_t stride, uint8_t size, uint8_t type);

int set_vertex_data_2f(uint32_t *fifo, uint8_t reg, const float val[2]);

int set_vertex_data_4f(uint32_t *fifo, uint8_t reg, const float val[4]);

int set_vertex_data_arr(uint32_t *fifo, uint8_t reg,
    uint16_t freq, uint8_t stride, uint8_t size, uint8_t type,
    uint8_t location, uint32_t offset);

int draw_arrays(uint32_t *fifo, uint32_t mode, uint32_t first, uint32_t count);

int invalidate_vertex_cache(uint32_t *fifo);

int set_texture(uint32_t *fifo, uint8_t index, const struct texture_desc *td);

int texture_ctrl(uint32_t *fifo, uint8_t index, uint8_t enable,
    uint16_t min_lod, uint16_t max_lod, uint8_t max_aniso);

int set_texture_addr(uint32_t *fifo, uint8_t index,
    uint8_t wrap_s, uint8_t wrap_t, uint8_t wrap_r, uint8_t unsigned_remap,
    uint8_t zfunc, uint8_t gamma);

int set_texture_filter(uint32_t *fifo, uint8_t index,
    uint16_t bias, uint8_t min, uint8_t mag, uint8_t conv);

int invalidate_texture_cache(uint32_t *fifo, uint32_t val);

int set_timestamp(uint32_t *fifo, uint32_t index);

#endif
