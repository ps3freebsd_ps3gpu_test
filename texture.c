/*-
 * Copyright (C) 2011, 2012 glevand <geoffrey.levand@mail.ru>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer,
 *    without modification, immediately at the beginning of the file.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 * NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 * THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD$
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stddef.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/uio.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/fbio.h>
#include <sys/consio.h>
#include <fcntl.h>
#include <unistd.h>

#include "ps3gpu_ctl.h"
#include "ps3gpu_mth.h"
#include "reset_gpu_state.h"
#include "util.h"
#include "matrix.h"

#define TEXTURE_WIDTH	512
#define TEXTURE_HEIGHT	512
#define TEXTURE_BPP	4
#define TEXTURE_PITCH	(TEXTURE_WIDTH * TEXTURE_BPP)

struct vertex {
	float x, y, z;
	float u, v;
};

int
main(int argc, char **argv)
{
	struct ps3gpu_ctl_context_allocate context_allocate;
	struct ps3gpu_ctl_context_free context_free;
	int context_id;
	volatile uint32_t *control;
	volatile uint8_t *driver_info;
	uint32_t *fifo, *reset_gpu, *db[2], *zb, *fp, *verts, *tex;
	unsigned long fifo_handle, reset_gpu_handle, db_handle[2], zb_handle, fp_handle, verts_handle, tex_handle;
	unsigned int fifo_gaddr, reset_gpu_gaddr, db_gaddr[2], zb_gaddr, fp_gaddr, verts_gaddr, tex_gaddr;
	matrix_t mat_proj, mat_view, mat;
	int fd = -1;
	int i, j, err;

	/* Open GPU device */

	fd = open(PS3GPU_DEV_PATH, O_RDWR);
	if (fd < 0) {
		perror("open");
		goto done;
	}

	/* Create GPU context */

	context_allocate.vram_size = 64; /* MB */

	err = ioctl(fd, PS3GPU_CTL_CONTEXT_ALLOCATE, &context_allocate);
	if (err < 0) {
		perror("ioctl");
		goto done;
	}

	context_id = context_allocate.context_id;

	printf("context id %d\n", context_id);
	printf("control handle 0x%lx size %d\n",
	    context_allocate.control_handle, context_allocate.control_size);
	printf("driver_info handle 0x%lx size %d\n",
	    context_allocate.driver_info_handle, context_allocate.driver_info_size);

	/* Map control registers */

	control = mmap(NULL, context_allocate.control_size,
	    PROT_READ | PROT_WRITE, MAP_SHARED, fd, context_allocate.control_handle);
	if (control == (void *) MAP_FAILED) {
		perror("mmap");
		goto done;
	}

	/* Map driver info */

	driver_info = mmap(NULL, context_allocate.driver_info_size,
	    PROT_READ | PROT_WRITE, MAP_SHARED, fd, context_allocate.driver_info_handle);
	if (driver_info == (void *) MAP_FAILED) {
		perror("mmap");
		goto done;
	}

	printf("channel id %d\n", get_channel_id(driver_info));

	/* Allocate FIFO */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_GART,
	    64 * 1024, 12, &fifo_handle, &fifo_gaddr, (void **) &fifo);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("FIFO handle 0x%lx gpu addr 0x%08x\n",
	    fifo_handle, fifo_gaddr);

	/* Setup FIFO */
	
	err = setup_control(fd, context_id, fifo_handle, fifo_handle, 0xdeadbabe);
	if (err < 0) {
		perror("setup_control");
		goto done;
	}

	printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
	    control[0x10], control[0x11], control[0x12]);

	/* Allocate FIFO for resetting GPU state */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_GART,
	    4 * 1024, 12, &reset_gpu_handle, &reset_gpu_gaddr, (void **)&reset_gpu);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("reset GPU state handle 0x%lx gpu addr 0x%08x\n",
	    reset_gpu_handle, reset_gpu_gaddr);

	memcpy(reset_gpu, reset_gpu_state_3d, reset_gpu_state_3d_size);

	/* Kick FIFO */

	fifo[0] = PS3GPU_MTH_HDR(0, 0, reset_gpu_gaddr | PS3GPU_MTH_ADDR_CALL);
	fifo[1] = PS3GPU_MTH_HDR(1, 0, PS3GPU_MTH_ADDR_REF);
	fifo[2] = 0xcafef00d;

	control[0x10] = fifo_gaddr + 3 * sizeof(uint32_t);

	err = wait_fifo_idle(control);
	if (err < 0) {
		fprintf(stderr, "FIFO timeout: put 0x%08x get 0x%08x ref 0x%08x\n",
		    control[0x10], control[0x11], control[0x12]);
		dump_fifo(stderr, fifo, 0x400);
		goto done;
	}

	printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
	    control[0x10], control[0x11], control[0x12]);

	/* Allocate display buffers */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_VIDEO,
	    ROUNDUP(DISPLAY_HEIGHT * DISPLAY_PITCH, 4 * 1024), 12,
	    &db_handle[0], &db_gaddr[0], (void **) &db[0]);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("DB0 handle 0x%lx gpu addr 0x%08x\n",
	    db_handle[0], db_gaddr[0]);

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_VIDEO,
	    ROUNDUP(DISPLAY_HEIGHT * DISPLAY_PITCH, 4 * 1024), 12,
	    &db_handle[1], &db_gaddr[1], (void **) &db[1]);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("DB1 handle 0x%lx gpu addr 0x%08x\n",
	    db_handle[1], db_gaddr[1]);

	/* Allocate depth buffer */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_VIDEO,
	    ROUNDUP(DISPLAY_HEIGHT * DISPLAY_PITCH, 4 * 1024), 12,
	    &zb_handle, &zb_gaddr, (void **) &zb);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("ZB handle 0x%lx gpu addr 0x%08x\n",
	    zb_handle, zb_gaddr);

	/* Allocate fragment program */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_VIDEO,
	    4 * 1024, 12, &fp_handle, &fp_gaddr, (void **) &fp);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("FP handle 0x%lx gpu addr 0x%08x\n",
	    fp_handle, fp_gaddr);

	/* Allocate vertices */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_VIDEO,
	    4 * 1024, 12, &verts_handle, &verts_gaddr, (void **) &verts);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("VERTS handle 0x%lx gpu addr 0x%08x\n",
	    verts_handle, verts_gaddr);

	/* Allocate texture */

	err = memory_allocate(fd, context_id, PS3GPU_CTL_MEMORY_TYPE_VIDEO,
	    ROUNDUP(TEXTURE_HEIGHT * TEXTURE_PITCH, 4 * 1024), 12,
	    &tex_handle, &tex_gaddr, (void **) &tex);
	if (err < 0) {
		perror("memory_allocate");
		goto done;
	}

	printf("TEX handle 0x%lx gpu addr 0x%08x\n",
	    tex_handle, tex_gaddr);

	/* Create checker texture */

	for (i = 0; i < TEXTURE_HEIGHT; i++) {
		for (j = 0; j < TEXTURE_WIDTH; j++) {
			if (((i & 0x80) == 0) ^ ((j & 0x80) == 0))
				*(tex + i * TEXTURE_WIDTH + j) = 0xff000000;
			else
				*(tex + i * TEXTURE_WIDTH + j) = 0xff00ff00;
		}
	}

	/* Set display buffers */

	err = display_buffer_set(fd, context_id, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT,
	    DISPLAY_PITCH, db_handle[0]);
	if (err < 0) {
		perror("display_buffer_set");
		goto done;
	}

	err = display_buffer_set(fd, context_id, 1, DISPLAY_WIDTH, DISPLAY_HEIGHT,
	    DISPLAY_PITCH, db_handle[1]);
	if (err < 0) {
		perror("display_buffer_set");
		goto done;
	}

	const struct surface_desc surf_desc[] = {
		/* display buffer 0 */
		{
			.sd_color_loc = { 0xfeed0000, 0xfeed0000, 0xfeed0000, 0xfeed0000 },
			.sd_color_off = { db_gaddr[0], 0, 0, 0 },
			.sd_color_pitch = { DISPLAY_PITCH, 64, 64, 64 },
			.sd_color_fmt = 0x8,
			.sd_color_target = 0x1,
			.sd_depth_loc = 0xfeed0000,
			.sd_depth_off = zb_gaddr,
			.sd_depth_pitch = DISPLAY_PITCH,
			.sd_depth_fmt = 0x2,
			.sd_x = 0,
			.sd_y = 0,
			.sd_w = DISPLAY_WIDTH,
			.sd_h = DISPLAY_HEIGHT,
		},
		/* display buffer 1 */
		{
			.sd_color_loc = { 0xfeed0000, 0xfeed0000, 0xfeed0000, 0xfeed0000 },
			.sd_color_off = { db_gaddr[1], 0, 0, 0 },
			.sd_color_pitch = { DISPLAY_PITCH, 64, 64, 64 },
			.sd_color_fmt = 0x8,
			.sd_color_target = 0x1,
			.sd_depth_loc = 0xfeed0000,
			.sd_depth_off = zb_gaddr,
			.sd_depth_pitch = DISPLAY_PITCH,
			.sd_depth_fmt = 0x2,
			.sd_x = 0,
			.sd_y = 0,
			.sd_w = DISPLAY_WIDTH,
			.sd_h = DISPLAY_HEIGHT,
		},
	};

	const struct texture_desc tex_desc = {
		.td_loc = 0x1,
		.td_off = tex_gaddr,
		.td_pitch = TEXTURE_PITCH,
		.td_w = TEXTURE_WIDTH,
		.td_h = TEXTURE_HEIGHT,
		.td_fmt = 0xa5,
		.td_border = 0x1,
		.td_depth = 0x1,
		.td_dimension = 0x2,
		.td_mipmap = 0x1,
		.td_cubemap = 0x0,
		.td_remap = (0x2 << 14) | (0x2 << 12) | (0x2 << 10) | (0x2 << 8) |
		    (0x3 << 6) | (0x2 << 4) | (0x1 << 2) | (0x0 << 0),
	};

	const uint32_t clear_color[] = {
		0xff404040,
		0xffffffff,
	};

	const float vp_offset[] = { DISPLAY_WIDTH * 0.5f, DISPLAY_HEIGHT * 0.5f, 0.5f, 0.0f };
	const float vp_scale[] = { DISPLAY_WIDTH * 0.5f, DISPLAY_HEIGHT * 0.5f, 0.5f, 0.0f };

	const uint32_t vertex_prg[] = {
		/* MOV o[7].xy, v[8].xy-- */
		0x401f9c6c, 0x00400808, 0x0106c083, 0x60419f9c,
		/* DP4 o[0].w, v[0], c[259] */
		0x401f9c6c, 0x01d0300d, 0x8106c0c3, 0x60403f80,
		/* DP4 o[0].z, v[0], c[258] */
		0x401f9c6c, 0x01d0200d, 0x8106c0c3, 0x60405f80,
		/* DP4 o[0].y, v[0], c[257] */
		0x401f9c6c, 0x01d0100d, 0x8106c0c3, 0x60409f80,
		/* DP4 o[0].x, v[0], c[256] */
		0x401f9c6c, 0x01d0000d, 0x8106c0c3, 0x60411f81,
	};

	uint32_t frag_prg[] = {
		/* TEX R0, f[4].xy--, TEX0, 2D */
		0x9e011700, 0xc8011c9d, 0xc8000001, 0xc8003fe1,
	};

	/*
	 * (-0.5, -0.5) -------- (0.5, -0.5)
	 *             |        |
	 *             |        |
	 *             |        |
	 *             |        |
	 * (-0.5, 0.5)  -------- (0.5, 0.5)
	 *
	 */

	const struct vertex quad_verts[] = {
		/* xyzuv */
		{  0.5f, -0.5f, -1.0f, 1.0f, 0.0f },
		{ -0.5f, -0.5f, -1.0f, 0.0f, 0.0f },
		{ -0.5f,  0.5f, -1.0f, 0.0f, 1.0f },
		{  0.5f,  0.5f, -1.0f, 1.0f, 1.0f },
	};

	err = setup_control(fd, context_id, fifo_handle, fifo_handle, 0xdeadbabe);
	if (err < 0) {
		perror("setup_control");
		goto done;
	}

	printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
	    control[0x10], control[0x11], control[0x12]);

	/* Transfer fragment program to VRAM */

	err += transfer_inline(fifo + err, 0xfeed0000, fp_gaddr,
	    frag_prg, ARRAY_SIZE(frag_prg));

	/* Transfer vertices to VRAM */

	err += transfer_inline(fifo + err, 0xfeed0000, verts_gaddr,
	    (uint32_t *) quad_verts, sizeof(quad_verts) / sizeof(uint32_t));

	control[0x10] = fifo_gaddr + err * sizeof(uint32_t);

	err = wait_fifo_idle(control);
	if (err < 0) {
		fprintf(stderr, "FIFO timeout: put 0x%08x get 0x%08x ref 0x%08x\n",
		    control[0x10], control[0x11], control[0x12]);
		dump_fifo(stderr, fifo, 0x400);
		goto done;
	}

	printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
	    control[0x10], control[0x11], control[0x12]);

	for (i = 0; i < ARRAY_SIZE(surf_desc); i++) {
		err = setup_control(fd, context_id, fifo_handle, fifo_handle, 0xdeadbabe);
		if (err < 0) {
			perror("setup_control");
			goto done;
		}

		printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
		    control[0x10], control[0x11], control[0x12]);

		err += set_surface(fifo + err, &surf_desc[i]);
		err += set_depth_mask(fifo + err, 0x00000000);
		err += set_color_mask(fifo + err, 0x01010101);
		err += set_color_mask_mrt(fifo + err, 0x00000000);
		err += set_clear_color(fifo + err, clear_color[i]);
		err += set_scissor(fifo + err, 0, 0, 4095, 4095);
		err += blend_enable(fifo + err, 0);
		err += clear_surface(fifo + err, 0x000000f1);

		err += set_viewport(fifo + err, 0, 0, DISPLAY_WIDTH, DISPLAY_HEIGHT,
		    0.0f, 1.0f, vp_offset, vp_scale);

		/* Set texture */

		err += invalidate_texture_cache(fifo + err, 0x1);
		err += set_texture(fifo + err, 0, &tex_desc);
		err += texture_ctrl(fifo + err, 0, 1, (0 << 8), (12 << 8), 0);
		err += set_texture_addr(fifo + err, 0, 0x3, 0x3, 0x3, 0x0, 0x1, 0x0);
		err += set_texture_filter(fifo + err, 0, 0, 0x2, 0x2, 0x1);

		/* Compute MVP matrix */

		matrix_proj(mat_proj, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 10000.0f);
		matrix_trans(mat_view, 0.0f, 0.0f, -0.5f);
		MATRIX_ELEM(mat_view, 0, 0) = 1.0f / (16.0f / 9.0f);
		matrix_mult(mat, mat_proj, mat_view);

		/* Set vertex shader */

		err += load_vertex_prg(fifo + err, 0, vertex_prg, ARRAY_SIZE(vertex_prg) / 4);
		err += set_vertex_prg_start_slot(fifo + err, 0);
		err += set_vertex_prg_reg_count(fifo + err, 1);
		err += set_vertex_prg_const(fifo + err, 256, 16, mat);
		err += set_vertex_attr_inmask(fifo + err, (1 << 8) | (1 << 0));
		err += set_vertex_attr_outmask(fifo + err, (1 << 14));

		/* Set fragment shader */

		err += set_frag_prg(fifo + err, 0x1, fp_gaddr);
		err += frag_prg_ctrl(fifo + err, 2, 0, 0, 0, 0);

		err += set_front_poly_mode(fifo + err, 0x1b02);
		err += set_shade_mode(fifo + err, 0x1d01);

		/* register 0 - position */
		err += set_vertex_data_arr(fifo + err, 0, 0, sizeof(struct vertex),
		    3, 2, 0x0, verts_gaddr + offsetof(struct vertex, x));
		/* register 8 - texture0 */
		err += set_vertex_data_arr(fifo + err, 8, 0, sizeof(struct vertex),
		    2, 2, 0x0, verts_gaddr + offsetof(struct vertex, u));

		err += draw_arrays(fifo + err, 0x8, 0, ARRAY_SIZE(quad_verts));

		err += flip_display_buffer(fifo + err, get_channel_id(driver_info), i, 0);

		/*
		 * Label with index 0 (head 0) is set by LV1 to 0x00000000 when flip is complete.
		 * Let GPU wait for it.
		 */

		err += wait_label(fifo + err, 0, 0x00000000);

		control[0x10] = fifo_gaddr + err * sizeof(uint32_t);

		err = wait_fifo_idle(control);
		if (err < 0) {
			fprintf(stderr, "FIFO timeout: put 0x%08x get 0x%08x ref 0x%08x\n",
			    control[0x10], control[0x11], control[0x12]);
			dump_fifo(stderr, fifo, 0x400);
			goto done;
		}

		printf("FIFO put 0x%08x get 0x%08x ref 0x%08x\n",
		    control[0x10], control[0x11], control[0x12]);

		usleep(1000000);
	}

	save_image("image.argb", (const char *) db[0], DISPLAY_PITCH * DISPLAY_HEIGHT);

	/* Destroy GPU context */

	context_free.context_id = context_id;

	err = ioctl(fd, PS3GPU_CTL_CONTEXT_FREE, &context_free);
	if (err < 0) {
		perror("ioctl");
		goto done;
	}

done:

	if (fd >= 0)
		close(fd);

	/* Restore console */

	ioctl(0, SW_TEXT_80x25, NULL);

	exit(0);
}
